package models


type ResponseObj  struct {
        Items      []Item
}

type Item struct {
        Id int `json:"id"`
        Fullname string `json:"full_name"`
        Cloneurl string `json:"clone_url"`
        Language string `json:"language"`
}

type LanguageObj struct {
        Language  string `json:"language"`
        NumberOfRepos int `json:"repos_count"`
        ReposList []string `json:"repos_list"`
}

type Output struct {
        Languages []LanguageObj
}

type Response struct {
        Status  string `json:"status"`
        Message string `json:"message"`
}

type ResponseError struct {
        Message string `json:"message"`
}

