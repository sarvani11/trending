FROM golang:1.14


RUN apt-get update && apt-get install -y \
    vim \
    procps

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .
# Run Application
RUN go build -o main .

EXPOSE 7082

# Command to run the executable
CMD ["./main"]
