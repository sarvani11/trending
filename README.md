## Rest MicroService Application to retrieve languages list from popular GitHub repos in Golang

** Steps to deploy the service using docker **

* Clone the source code

    ```git clone https://sarvani11@bitbucket.org/sarvani11/trending.git```
    
    

* Modify the configuration file config.json like host address,server port and save it.


* Build the docker image

    ```docker build -t task```


* Start the container (by default service will be running on port 7082)

    ```docker run -d -it  -p 7082:7082  --name task  -t task```
    

* Connect to the container

    ```docker exec -it CONTAINER ID bash```
    

* To check services are running or not

    ```docker logs -f CONTAINER ID```


* Navigate to swagger UI for API documentation. The UI will be running on "/swagger/index.html" endpoint

    ```Ex: http://localhost:7082/swagger/index.html```

    ```NOTE: Change localhost to host address added in config.json. By default service will be running on port 7082```
    
* Refer Documentation.txt to know further information
