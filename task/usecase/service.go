package usecase

import (
	"time"
	"fmt"
	"log"
	"github.com/gemography/coding_challenge/task"
	"github.com/gemography/coding_challenge/models"
)

type ServiceUsecase struct {
	Repo         task.Repository
}


// NewUsecase will create an object that represent the ServiceUsecase 
func NewUsecase(repo task.Repository) task.Usecase {
	log.Println("ServiceUsecase Created")
	uc := ServiceUsecase{Repo: repo}
	return &uc
}

// Retrieve Languages list based on treding Github repos
func (uc *ServiceUsecase) GetLanguages() (models.Output, error) {
	now := time.Now()
	then := now.AddDate(0, 0, -30)
	fdate := fmt.Sprintf("%d-%02d-%02d", then.Year(), then.Month(), then.Day())
	fmt.Println("GetLanguages_Usecase: Picked date from now to last 30 days  ", fdate)
	emptyObj := models.Output{}
	response, err := uc.Repo.GetTrendingGitHubRepos(fdate)
	if err != nil {
		log.Println("GetLanguages_Usecase: Fetching api issue ", err)
		return emptyObj, err
	}
	data, err := uc.Repo.GetLanguages(response)
	if err != nil {
		log.Println("GetLanguages_Usecase: Fetching Languages issue ", err)
		return emptyObj, err
	}
	return data, nil
}
