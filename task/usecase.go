package task

import (
        "github.com/gemography/coding_challenge/models"
)

// interface
type Usecase interface {
        GetLanguages() (models.Output, error)
}
