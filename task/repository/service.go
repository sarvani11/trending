package serviceregistry

import (
	"encoding/json"
	"log"
	"fmt"
	"github.com/gemography/coding_challenge/task"
	"github.com/gemography/coding_challenge/models"
	"net/http"
        "io/ioutil"
)

type ServiceRepository struct {
}

// NewRepository will create an object that represent the ServiceRepository 
func NewRepository() task.Repository {
	log.Println("NewRepository Created")
	newRepo := ServiceRepository{}
	return &newRepo
}

// Retrieve Trending GitHub Repos in last 30 days from now
func (sr *ServiceRepository) GetTrendingGitHubRepos(dateparam string) (models.ResponseObj, error) {
	emptyObj := models.ResponseObj{}
	resp, err := http.Get("https://api.github.com/search/repositories?q=created:>" + dateparam + "&sort=stars&order=desc")
        if err != nil {
		log.Println("GetTrendingGitHubRepos: Fetching api issue ", err)
		return emptyObj, err
        }
        defer resp.Body.Close()
        bytesdata, err := ioutil.ReadAll(resp.Body)
        if err != nil {
		log.Println("GetTrendingGitHubRepos: Reading response issue", err)
		return emptyObj, err
        }
        var respObj models.ResponseObj
        err = json.Unmarshal([]byte(bytesdata), &respObj)
        if err != nil {
		log.Println("GetTrendingGitHubRepos: Unmarshaling issue ", err)
		return emptyObj, err
        }
	// According to Functional Spec, get 100 trending public repos on GitHub.
        if len(respObj.Items) >= 100 {
                temp := respObj.Items[:100]
                respObj.Items = temp
        }
	log.Println("GetTrendingGitHubRepos: Total number of starred repos created in last 30 days from now: ", len(respObj.Items))
	return respObj, nil
}

// Retrieve Languages from trending Github Repos
func (sr *ServiceRepository) GetLanguages(respObj models.ResponseObj) (models.Output, error) {
	out := models.Output{}
        tempmap := make(map[string]models.LanguageObj)
        for _, item := range respObj.Items {
		fmt.Println("GetLanguages: GitHub repo item ", item)
                if len(item.Language) != 0 {
                        if val, ok := tempmap[item.Language]; ok {
                                val.ReposList = append(val.ReposList, item.Cloneurl)
                                val.NumberOfRepos = val.NumberOfRepos + 1
                                tempmap[item.Language] = val
                        } else {
                                temp := models.LanguageObj{Language: item.Language, NumberOfRepos: 1, ReposList: []string{item.Cloneurl}}
                                tempmap[item.Language] = temp
                        }
                }
        }
        for _, item := range tempmap {
                out.Languages = append(out.Languages, item)
        }
	return out, nil
}
