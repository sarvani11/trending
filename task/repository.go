package task

import (
	"github.com/gemography/coding_challenge/models"
)

// interface
type Repository interface {
        GetTrendingGitHubRepos(dateparam string) (models.ResponseObj, error)
        GetLanguages(resp models.ResponseObj) (models.Output, error)
}
