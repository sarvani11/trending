package http

import (
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
        "github.com/stretchr/testify/mock"

	"github.com/gemography/coding_challenge/mocks"
	"github.com/gemography/coding_challenge/models"
	svchttp "github.com/gemography/coding_challenge/task/delivery/http"
)

func TestGetLanguages(t *testing.T) {
	mockmodel := models.Output{}
	mockUc := new(mocks.Usecase)
	mockUc.On("GetLanguages", mock.Anything).Return(mockmodel, nil)
	e := echo.New()
	req, err := http.NewRequest("GET", "/languages", nil)
	if err != nil {
		t.Fatal(err)
	}
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	hdlr := &svchttp.Handler{
                UCase: mockUc,
	}
        if assert.NoError(t, hdlr.GetLanguages(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		log.Println(rec.Code)
		log.Println(rec.Body.String())
	}
        mockUc.AssertExpectations(t)
}
