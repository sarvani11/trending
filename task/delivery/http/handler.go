package http

import (
	"net/http"
	"github.com/labstack/echo/v4"
	"github.com/gemography/coding_challenge/task"
	"github.com/gemography/coding_challenge/models"
)

type Handler struct {
        UCase task.Usecase
}

// NewHandler will initialize the  endpoint resources
func NewHandler(e *echo.Echo, uc task.Usecase) {
        handler := &Handler{
                UCase: uc,
        }
	e.GET("/healthcheck", handler.HealthCheck)
        e.GET("/languages", handler.GetLanguages)
}


// @Summary Get Languages
// @Description Get Languages from trending GitHub Repos
// @Tags Task
// @Produce json
// @Success 200 {object} models.Output
// @Failure 400 {object} models.ResponseError
// @Failure 404 {object} models.ResponseError
// @Failure 500 {object} models.ResponseError
// @Router /languages [get]
func (handler *Handler) GetLanguages(c echo.Context) error {
	response, err := handler.UCase.GetLanguages()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, models.ResponseError{Message: err.Error()})
	}
	return c.JSON(http.StatusOK, response)
}

// Healthcheck
// @Description Get Healthcheck
// @Tags Healthcheck
// @Summary Get Healthcheck
// @Produce json
// @Success 200 {object} models.Response
// @Router /healthcheck [get]
func (handler *Handler) HealthCheck(c echo.Context) error {
	return c.JSON(http.StatusOK, models.Response{Status: "Success", Message: "API Healthcheck OK"})
}
