package main

import (
	"log"
	"time"
	"net/http"
	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
	"github.com/gemography/coding_challenge/docs"


	"github.com/swaggo/echo-swagger"
	httpdeliver "github.com/gemography/coding_challenge/task/delivery/http"
        Repo "github.com/gemography/coding_challenge/task/repository"
	Uc "github.com/gemography/coding_challenge/task/usecase"
)

func init() {
	viper.SetConfigFile("config.json")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

// @title Swagger Languages API
// @version 1.0
// @description Languages API.
// @termsOfService http://swagger.io/terms/
// @contact.name Sarvani
// @contact.email sarvanimounika@gmail.com
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host 63.49.193.10:443
// @BasePath /

func main() {
	e := echo.New()
	docs.SwaggerInfo.Host = viper.GetString("host.address") + viper.GetString("server.port")
        e.GET("/swagger/*", echoSwagger.WrapHandler)
	repoObj := Repo.NewRepository()

	ucObj := Uc.NewUsecase(repoObj)
	httpdeliver.NewHandler(e, ucObj)

	s := &http.Server{
		Addr:         viper.GetString("server.port"),
		ReadTimeout:  20 * time.Minute,
		WriteTimeout: 20 * time.Minute,
	}
	log.Fatal(e.StartServer(s))
}
